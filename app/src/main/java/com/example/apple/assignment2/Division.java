package com.example.apple.assignment2;

/**
 * Created by apple on 29/04/2016.
 */
public class Division extends Expression{
    Expression leftExpression;
    Expression rightExpression;
    public Division(Expression lx, Expression rx) {
        this.leftExpression = lx;
        this.rightExpression = rx;
    }

    @Override
    public String show() {
        return null;
    }

    @Override
    public float evaluate() {
        return leftExpression.evaluate() / rightExpression.evaluate();
    }
}
