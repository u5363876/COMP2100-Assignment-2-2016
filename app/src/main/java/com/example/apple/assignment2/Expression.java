package com.example.apple.assignment2;


import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by apple on 29/04/2016.
 */
public abstract class Expression {

    static MainActivity ma = new MainActivity();
    static String errorMessage = "";
    /*  This method prints an expression as a string
            (which could be parsed back into a expression) */
    public abstract String show();
    /* This method evaluates the expression */
    public abstract float evaluate();

    //(10+9)*(9-(2*3)/(100-20))
    //"-1+3*(3-9)/20+9"
    static public Expression parse2(String input) throws Exception {


        System.out.println(input);
        input = removeOuterBracket(input);


        //deal with Number
        if (isStringNumeric(input)){
            float f = Float.parseFloat(input);
            return new Number(f);
        }



        List<String> splittedString = toList(input);
        System.out.println(splittedString);

        List<String> splitdString2 = new ArrayList<String>();
        try {
            splitdString2 = mergeUnary(splittedString);
        }catch (Exception e){
            System.out.println("Exception caught");
        }
        System.out.println(splitdString2);




        //toexpression
        int operationIndex = 0;  //at this point, devide expression into two. Run parse2 on both size.

        if (splitdString2.indexOf("+") > 0){//if +
            operationIndex = splitdString2.indexOf("+");
            return parseLeftNRight(splitdString2, operationIndex, "+");
        }else if (splitdString2.indexOf("-") > 0){//if -
            operationIndex = splitdString2.indexOf("-");
            return parseLeftNRight(splitdString2, operationIndex, "-");
        }else if (splitdString2.indexOf("*") > 0){//if *
            operationIndex = splitdString2.indexOf("*");
            return parseLeftNRight(splitdString2, operationIndex, "*");
        }else if (splitdString2.indexOf("/") > 0){//if /
            operationIndex = splitdString2.indexOf("/");
            return parseLeftNRight(splitdString2, operationIndex, "/");
        }

        return new Number(0);
    }


    public static Expression parseLeftNRight(List<String> expList, int opIndex, String op) throws Exception {
        String strLeft = merge(expList.subList(0, opIndex));
        String strRight = merge(expList.subList(opIndex+1, expList.size()));

        Expression returnVal = new Number(0);//this value will never be used.
        switch (op){
            case "+":
                returnVal = new Addition(parse2(strLeft), parse2(strRight));
                break;
            case "-":
                returnVal = new Minus(parse2(strLeft), parse2(strRight));
                break;
            case "*":
                returnVal = new Multiply(parse2(strLeft), parse2(strRight));
                break;
            case "/":
                returnVal = new Division(parse2(strLeft), parse2(strRight));
                break;
        }
        return returnVal;
    }


    public static String merge(List<String> expList){
        String returnVal = "";
        for (String item:expList) {
            returnVal += item;
        }
        return returnVal;
    }

    public static List<String> toList(String input){
        String currentNumber = "";
        String bracketHold = "";
        int bracketBalance = 0;
        List<String> splittedExpression = new ArrayList<String>();
        //string to list of num and expression
        char ch;
        for (int i = 0; i < input.length(); i++) {
            ch = input.charAt(i);

            switch (ch) {
                case '(':
                    bracketBalance += 1;
                    break;
                case ')':
                    bracketBalance -= 1;
                    break;
                default:
                    break;
            }


            if (bracketBalance >= 1) {
                bracketHold += ch;
                continue;
            } else if (bracketBalance == 0 && bracketHold.length() > 0) { //end of a pair of bracket, and not empty inside
                splittedExpression.add(bracketHold + ")"); //format will be '(expression)'
                bracketHold = "";
                continue;
            }


            //operator
            if (ch == '+' || ch == '-' || ch == '*' || ch == '/') {
                if (currentNumber.length() > 0){
                    splittedExpression.add(currentNumber);
                    currentNumber = "";
                }
                splittedExpression.add(String.valueOf(ch));
            } else {//number
                currentNumber += ch;
                //if the last one, just append the rest
                if (i == input.length()-1){
                    splittedExpression.add(currentNumber);
                }
            }
        }
        return splittedExpression;
    }

    public static String removeOuterBracket(String input){
        int bracketBalance = 1;//count as one '(' braces
        int loversIndex = -1;
        //incase bracket at both front and back, remove both.
        if (input.charAt(0) == '(' && input.charAt(input.length()-1) == ')'){
            //check if they are a pair

            //check the index of ) for the first (
            char elem;
            for (int i = 1; i < input.length(); i++) {
                elem = input.charAt(i);
                if (elem == '('){
                    bracketBalance += 1;
                }else if (elem == ')'){
                    bracketBalance -= 1;
                }

                if (bracketBalance == 0){
                    //fit found
                    loversIndex = i;
                    break;
                }
            }

            if (loversIndex == (input.length()-1)){
                input = input.substring(1, input.length()-1);
            }//otherwise ignore
        }
        return input;
    }

    public static List<String> mergeUnary(List<String> rawList) throws Exception {

        String operatorHold = "";
        int operatorBal = 0;
        List<String> splitdExp2 = new ArrayList<String>();

        //operator
        String elem = "";
        for (int i = 0; i < rawList.size(); i++) {
            elem = rawList.get(i);
            if (elem.equals("+") || elem.equals("-") || elem.equals("*") || elem.equals("/") ) {
                operatorBal -= 1;
            }else{//num or bracket
                operatorBal += 1;
            }

            //operatorHold will only be empty if unary is combined.
            //OperatorBal is 0 after regular operator or 0 after regular elem with unary operator
            //operatorBal is -1 when operatorHold is holding that unary
            //opratorBal is 1 when it's regular
            if (operatorBal == 1 && operatorHold == ""){//a regular non-op elem.
                splitdExp2.add(elem);
            }else if (operatorHold != "" && operatorBal == 0) {//there was a unary sign before this item. otherwise opratorBal is 1
                switch (operatorHold){
                    case "-":
                        if (isStringNumeric(elem)){//num
                            splitdExp2.add("-" + elem);
                        }else{//expression
                            splitdExp2.add("-1");
                            splitdExp2.add("*");
                            splitdExp2.add(elem);
                        }
                        break;
                    case "+":
                        splitdExp2.add(elem);
                        break;
                    default:
                        errorMessage = "having * or / as unary is not allowed";
                        throw new Exception("having * or / as unary is not allowed");
                        //break;
                }
                operatorBal = 1;//1 presents thers element before it, not op
                operatorHold = "";
            }else if (operatorBal == 0 && (Arrays.asList("+", "-", "*", "/").contains(elem))){ //regular op elem
                splitdExp2.add(elem);
            }else if (operatorBal == -1){ //the unary op
                operatorHold += elem;
            }else{
                errorMessage = "Check Your Expression";
                throw new Exception("Check Your Expression");
            }



            if(operatorBal < -1){
                errorMessage = "Check your operators";
                throw new Exception("Check your operators");
            }
        }
        return splitdExp2;
    }


    //ref
    //http://stackoverflow.com/questions/1102891/how-to-check-if-a-string-is-numeric-in-java
    public static boolean isStringNumeric( String str )
    {
        DecimalFormatSymbols currentLocaleSymbols = DecimalFormatSymbols.getInstance();
        char localeMinusSign = currentLocaleSymbols.getMinusSign();

        if ( !Character.isDigit( str.charAt( 0 ) ) && str.charAt( 0 ) != localeMinusSign ) return false;

        boolean isDecimalSeparatorFound = false;
        char localeDecimalSeparator = currentLocaleSymbols.getDecimalSeparator();

        for ( char c : str.substring( 1 ).toCharArray() )
        {
            if ( !Character.isDigit( c ) )
            {
                if ( c == localeDecimalSeparator && !isDecimalSeparatorFound )
                {
                    isDecimalSeparatorFound = true;
                    continue;
                }
                return false;
            }
        }
        return true;
    }
}
