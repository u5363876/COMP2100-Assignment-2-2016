package com.example.apple.assignment2;

/**
 * Created by apple on 29/04/2016.
 */
public class Number extends Expression {
    float fn = 0;
    public Number(float number) {
        this.fn = number;
    }

    @Override
    public String show() {
        return null;
    }

    @Override
    public float evaluate() {
        return fn;
    }
}
