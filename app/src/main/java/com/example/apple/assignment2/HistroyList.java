package com.example.apple.assignment2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class HistroyList extends AppCompatActivity {
    String history = "";
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histroy_list);

        //initialisation
        textView = (TextView) findViewById(R.id.HistoryView);

        try {
            InputStream inputStream = openFileInput(MainActivity.filename);
            if(inputStream != null){
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiving = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiving = bufferedReader.readLine()) != null){
                    stringBuilder.append(receiving + "\n");
                }

                inputStream.close();
                history = stringBuilder.toString();
            }
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        textView.setText(history);
    }

    public void back (View vw){
        startActivity(new Intent(HistroyList.this, MainActivity.class));
    }
}
