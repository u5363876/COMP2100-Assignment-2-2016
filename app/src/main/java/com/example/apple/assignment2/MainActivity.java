package com.example.apple.assignment2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView textField;
    String str = "";
    String errorMessage = "";
    public static String filename = "History";
    public Context context;
    Button hButton;
    static ArrayList<String> exps = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Expression exp = Expression.parse2("(10+9)*(9-(2*3)/(100-20))");

        //initialisation
        textField = (TextView) findViewById(R.id.textView);
        hButton = (Button) findViewById(R.id.history);
        hButton.setOnClickListener(new myClass());
        context = getBaseContext();

    }





    public void equal(View v) throws Exception {
        if (legitInput(str)){
            saveHistory();

            if(str.contains("a")){
                String result2 = average(str);
                textField.setText(result2);
            }
            else{
                Expression exp = Expression.parse2(str);
                float result = exp.evaluate();
                str = Float.toString(result);
                textField.setText(str);
            }

        }else{
            //Empty input warning.
            //------------By Kelly_Kang ----
            Toast toast = Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
            //------------- End ------
        }
    }

    public boolean legitInput(String input){
        if (input.length() == 0){
            errorMessage = "No Expression";
            return false;
        }else{
            int bBal = 0; //bracket balance.
            //bracket check.
            for (char ch:input.toCharArray()) {
                if (ch == '('){
                    bBal += 1;
                }else if (ch == ')'){
                    bBal -= 1;
                }
                if (bBal < 0 ){
                    errorMessage = "Check your brackets";
                    return false;
                }
            }
            if (bBal != 0){
                errorMessage = "Unbalanced brackets";
                return false;
            }
        }

        return true;
    }

    //for delete button
    public void undoType (View vw){
        if(str.length() > 0){
            str = str.substring(0, str.length() - 1);
            textField.setText(str);
        }
    }


    //display user equation in textField
    //get the string
    public  void onClick (View vw){
        switch (vw.getId()){
            case R.id.zero:
                str = str.concat("0");
                textField.setText(str);
                break;
            case R.id.one:
                str = str.concat("1");
                textField.setText(str);
                break;
            case R.id.two:
                str = str.concat("2");
                textField.setText(str);
                break;
            case R.id.three:
                str = str.concat("3");
                textField.setText(str);
                break;
            case R.id.four:
                str = str.concat("4");
                textField.setText(str);
                break;
            case R.id.five:
                str = str.concat("5");
                textField.setText(str);
                break;
            case R.id.six:
                str = str.concat("6");
                textField.setText(str);
                break;
            case R.id.seven:
                str = str.concat("7");
                textField.setText(str);
                break;
            case R.id.eight:
                str = str.concat("8");
                textField.setText(str);
                break;
            case R.id.nine:
                str = str.concat("9");
                textField.setText(str);
                break;
            case R.id.dot:
                str = str.concat(".");
                textField.setText(str);
                break;
            case R.id.minus:
                str = str.concat("-");
                textField.setText(str);
                break;
            case R.id.plus:
                str = str.concat("+");
                textField.setText(str);
                break;
            case R.id.delete:
                //http://stackoverflow.com/questions/7438612/how-to-remove-the-last-character-from-a-string
                if (str != null && str.length() > 0 && str.charAt(str.length()-1)=='x') {
                    str = str.substring(0, str.length()-1);
                }
                textField.setText(str);
                break;
            case R.id.divide:
                str = str.concat("/");
                textField.setText(str);
                break;
            case R.id.LB:
                str = str.concat("(");
                textField.setText(str);
                break;
            case R.id.RB:
                str = str.concat(")");
                textField.setText(str);
                break;
            case R.id.multiply:
                str = str.concat("*");
                textField.setText(str);
                break;
            case R.id.average:
                str = str.concat("a");
                textField.setText(str);
                break;
            default:
                System.out.println("No button id found ");
                break;
        }
    }


    public String average(String s){
        String[] list = s.split("a");
        Float a = Float.parseFloat(list[0]);
        Float b = Float.parseFloat(list[1]);
        double result = (a + b) * 0.5;

        return Double.toString(result);

    }

    public void saveHistory (){
        exps.add(str);
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(filename, Context.MODE_PRIVATE));
            BufferedWriter writer = new BufferedWriter(outputStreamWriter);
            for(int i = 0; i < exps.size(); i++){
                writer.append(exps.get(i));
                writer.newLine();
            }
            writer.close();
            outputStreamWriter.close();

            System.out.println(str);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

    }

    public class myClass implements View.OnClickListener{


        @Override
        public void onClick(View v) {
            startActivity(new Intent(MainActivity.this, HistroyList.class));
        }
    }

}






